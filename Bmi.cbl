       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. PUBODIN.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  HEIGHT   PIC 999V99 .
       01  WEIGHT   PIC 999V99.
       01  BMI      PIC 99V99.
       01  BMI-TYPE PIC X(15).

       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "Input height (cm) - " WITH NO ADVANCING 
           ACCEPT HEIGHT 
           DISPLAY "Input weight (kg) - " WITH NO ADVANCING 
           ACCEPT WEIGHT 
           COMPUTE BMI = WEIGHT / (HEIGHT /100)**2
           END-COMPUTE 
           DISPLAY "Bmi score is " BMI
           IF BMI < 18.5 THEN
              DISPLAY "UNDERWEIGHT"
           END-IF 
           IF BMI >= 18.5 AND BMI <= 24.9
              DISPLAY "NORMAL"
           END-IF 
           IF BMI >= 25.0 AND BMI <= 29.9
              DISPLAY "OVERWEIGHT"
           END-IF 
           IF BMI >= 30.0 AND BMI <= 34.9
              DISPLAY "OBESE"
           END-IF 
           IF BMI >=35.0
              DISPLAY "EXTREMLY OBESE"
           END-IF 
       .
